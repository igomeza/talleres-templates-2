package mundo;

public class Computador extends Electronico {

	public enum OS{
		Windows, OSX, ChromeOS, Ubuntu
	}
	private OS sistemaOperativo ;
	public Computador(Gama pGama, double pPrecio, OS pOs){
		super(pGama, pPrecio);
		sistemaOperativo=pOs;
	}
	public String toString(){
		return "Computador " + sistemaOperativo.name() + " - "+ "Gama " + gama; 
	}
}


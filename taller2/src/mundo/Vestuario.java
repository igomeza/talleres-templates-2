package mundo;

public class Vestuario extends Producto{
	
	public enum Talla{
		
		XS, S, M, L, XL, XXL, XXXL
	}
	public enum Marca{
		GEF, POLO ,NAUTICA
	}
	
	protected Talla talla;
	
	public Vestuario(Talla pTalla, double pPrecio)
	{
		super(pPrecio);
		talla = pTalla;
		
	}

}
